<?php
/*
 * @Author: kaleozhou
 * @Email: kaleo1990@hotmail.com
 * @Blog: www.kaleozhou.top
 * @Date: 2021-05-20 22:06:14
 * @LastEditors: kaleozhou
 * @LastEditTime: 2021-05-21 07:52:28
 * @Description: content
 */

namespace Encore\UploadFileCos\Http\Controllers;

use Encore\Admin\Layout\Content;
use Illuminate\Routing\Controller;
use Encore\UploadFileCos\QcloudStsHandler;

class UploadFileCosController extends Controller
{
    public function index(Content $content)
    {
        $sts = new QcloudStsHandler();
        // 配置参数
        $config = array(
            'url' => 'https://sts.tencentcloudapi.com/',
            'domain' => 'sts.tencentcloudapi.com',
            'proxy' => '',
            'secretId' => config('filesystems.disks.cosv5.credentials.secretId'), // 固定密钥
            'secretKey' => config('filesystems.disks.cosv5.credentials.secretKey'), // 固定密钥
            'bucket' => config('filesystems.disks.cosv5.bucket'), // 换成你的 bucket
            'region' => config('filesystems.disks.cosv5.region'), // 换成 bucket 所在园区
            'durationSeconds' => 1800, // 密钥有效期
            // 允许操作（上传）的对象前缀，可以根据自己网站的用户登录态判断允许上传的目录，例子： user1/* 或者 * 或者a.jpg
            // 请注意当使用 * 时，可能存在安全风险，详情请参阅：https://cloud.tencent.com/document/product/436/40265
            'allowPrefix' => '*',
            // 密钥的权限列表。简单上传和分片需要以下的权限，其他权限列表请看 https://cloud.tencent.com/document/product/436/31923
            'allowActions' => array(
                // 所有 action 请看文档 https://cloud.tencent.com/document/product/436/31923
                // 简单上传
                'name/cos:PutObject',
                'name/cos:PostObject',
                // 分片上传
                'name/cos:InitiateMultipartUpload',
                'name/cos:ListMultipartUploads',
                'name/cos:ListParts',
                'name/cos:UploadPart',
                'name/cos:CompleteMultipartUpload'
            )
        );
        // 获取临时密钥，计算签名
        $tempKeys = $sts->getTempKeys($config);

        // echo json_encode($tempKeys);
        return response($tempKeys)
            ->header('Content-Type', 'application/json')
            ->header('Access-Control-Allow-Origin', url('/')) // 这里修改允许跨域访问的网站
            ->header('Access-Control-Allow-Headers', 'origin,accept,content-type');
    }
}
